INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('PADRE', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('MADRE', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('HIJO', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('ABUELA', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('ABUELO', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('HERMANO', NOW());
INSERT INTO rol_familiar (rol, fecha_creacion) VALUES ('TIO', NOW());
INSERT INTO nucleo_familiar (apellido, fecha_creacion) VALUES ("LOPEZ",NOW());
INSERT INTO usuario_app (nombre,contrasena) VALUES ("APPNEXOS"	,"$2a$10$d4qedY7HuV.o6oP0QEqzX.uKsc9XpGC9w6/HzJjDbNuOVpbDmsyoy")