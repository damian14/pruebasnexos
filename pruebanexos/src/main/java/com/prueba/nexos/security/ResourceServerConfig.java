package com.prueba.nexos.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfig  extends ResourceServerConfigurerAdapter{
	
	 private static final String RESOURCE_ID = "resource_id";
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// TODO Auto-generated method stub
		 resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.authorizeRequests().antMatchers("/oauth/token").permitAll()
		.antMatchers(HttpMethod.GET,"/v1/**").hasAnyRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/v1/**").hasAnyRole("ADMIN")
		.antMatchers(HttpMethod.PUT,"/v1/**").hasAnyRole("ADMIN")
		//.antMatchers(HttpMethod.GET, "/**").permitAll()
		;
	}
	

}
