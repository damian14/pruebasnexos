package com.prueba.nexos.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "nucleo_familiar")
public class NucleoFamiliar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idNucleoFamiliar;
	
	@Column(name = "apellido", unique = true, length = 30)
	private String apellido;
	
	@Column(name = "fecha_creacion")
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;

	

	public Integer getIdNucleoFamiliar() {
		return idNucleoFamiliar;
	}

	public void setIdNucleoFamiliar(Integer idNucleoFamiliar) {
		this.idNucleoFamiliar = idNucleoFamiliar;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
