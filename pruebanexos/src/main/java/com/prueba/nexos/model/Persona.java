package com.prueba.nexos.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "persona")
public class Persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	@Column(name = "nombre", length = 20)
	private String nombre;
	@Column(name = "apellido", length = 30)
	private String apellido;
	@Column(name = "genero",  length = 1)
	private Integer genero;
	@Column(name = "fecha_creacion")
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "persona_rolfamiliar", joinColumns = @JoinColumn(name = "id_persona"), inverseJoinColumns = @JoinColumn(name = "id_rol_familiar"), uniqueConstraints = {
			@UniqueConstraint(columnNames = { "id_persona", "id_rol_familiar" }) })
	private List<RolFamilia> rolesFamilia;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "persona_nucleo_familiar", joinColumns = @JoinColumn(name = "id_persona"), inverseJoinColumns = @JoinColumn(name = "id_nucleo_familiar"), uniqueConstraints = {
			@UniqueConstraint(columnNames = { "id_persona", "id_nucleo_familiar" }) })
	private List<NucleoFamiliar> nucleoFamiliar;

	public List<NucleoFamiliar> getNucleoFamiliar() {
		return nucleoFamiliar;
	}

	public void setNucleoFamiliar(List<NucleoFamiliar> nucleoFamiliar) {
		this.nucleoFamiliar = nucleoFamiliar;
	}

	public List<RolFamilia> getRolesFamilia() {
		return rolesFamilia;
	}

	public void setRolesFamilia(List<RolFamilia> rolesFamilia) {
		this.rolesFamilia = rolesFamilia;
	}

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getGenero() {
		return genero;
	}

	public void setGenero(Integer genero) {
		this.genero = genero;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
