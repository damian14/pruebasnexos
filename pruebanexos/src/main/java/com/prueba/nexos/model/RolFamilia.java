package com.prueba.nexos.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="rol_familiar")
public class RolFamilia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idRolFamilia;
	
	@Column(unique = true , length = 30)
	private String rol;
	
	@Column(name="fecha_creacion")
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;

	public Integer getIdRolFamilia() {
		return idRolFamilia;
	}

	public void setIdRolFamilia(Integer idRolFamilia) {
		this.idRolFamilia = idRolFamilia;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
