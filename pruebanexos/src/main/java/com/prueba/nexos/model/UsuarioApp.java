package com.prueba.nexos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_app")
public class UsuarioApp {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuarioApp;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "contrasena")
	private String contrasena;

	public Long getIdUsuarioApp() {
		return idUsuarioApp;
	}

	public void setIdUsuarioApp(Long idUsuarioApp) {
		this.idUsuarioApp = idUsuarioApp;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

}
