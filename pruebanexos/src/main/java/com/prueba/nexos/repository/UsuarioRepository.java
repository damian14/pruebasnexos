package com.prueba.nexos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prueba.nexos.model.UsuarioApp;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioApp, Long>{
	
	
	public UsuarioApp findByNombre(String nombre);
	
	

}
