package com.prueba.nexos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prueba.nexos.model.RolFamilia;

@Repository
public interface RolFamiliaRepository extends CrudRepository<RolFamilia, Integer>{

}
