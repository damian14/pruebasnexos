package com.prueba.nexos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prueba.nexos.model.NucleoFamiliar;

@Repository
public interface NucleoFamiliaRepository extends CrudRepository<NucleoFamiliar, Integer>{

}
