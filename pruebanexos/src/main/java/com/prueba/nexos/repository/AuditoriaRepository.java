package com.prueba.nexos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prueba.nexos.model.Auditoria;

@Repository
public interface AuditoriaRepository extends CrudRepository<Auditoria, Integer>{

}
