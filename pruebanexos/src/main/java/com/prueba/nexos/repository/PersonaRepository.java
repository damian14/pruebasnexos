package com.prueba.nexos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.prueba.nexos.model.Persona;

@Repository
public interface PersonaRepository  extends CrudRepository<Persona, Integer>{

}
