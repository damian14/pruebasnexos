package com.prueba.nexos.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.nexos.model.NucleoFamiliar;
import com.prueba.nexos.model.Persona;
import com.prueba.nexos.model.RolFamilia;
import com.prueba.nexos.service.INucleoFamiliarService;
import com.prueba.nexos.service.IPersonaService;
import com.prueba.nexos.service.IRolFamiliaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1")
public class NucleoFamiliarController {

	@Autowired
	private IPersonaService iPersonaService;
	
	@Autowired
	private IRolFamiliaService iRolFamiliaService;
	
	@Autowired
	private INucleoFamiliarService iNucleoFamiliarService;
	
	
	@ApiOperation(value = "Muestra todas las personas")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 404, message = "Búsqueda sin resultados"),
			@ApiResponse(code = 400, message = "Error al obtener personas") })
	@GetMapping("/buscarPersonas")
	public ResponseEntity<?>obtenerPersonas(HttpServletResponse response) throws IOException {
		return iPersonaService.obtenerPersonas(response);
	}
	
	
	@ApiOperation(value = "Crear una persona")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al crear persona") })
	@PostMapping("/crearPersona")
	public ResponseEntity<?> crearPersona(@RequestBody Persona persona,HttpServletResponse response) throws IOException {
		//return iPersonaService.obtenerPersonas()
		persona.setFechaCreacion(new Date());
		return iPersonaService.crearPersona(persona,response);
	}
	
	@ApiOperation(value = "Editar una persona")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al actualizar persona"),
			@ApiResponse(code = 404, message = "No existe personas con este id")})
	@PutMapping("/editarPersona/{id}")
	public ResponseEntity<?> editar(@RequestBody Persona persona, @PathVariable Integer id,HttpServletResponse response) throws IOException{
		ResponseEntity<?> per = iPersonaService.obtenerPersonaPorId(id, response);
		Persona personaEditar = (Persona) per.getBody();
		if(personaEditar != null) {
			personaEditar.setApellido(persona.getApellido()); 
			 personaEditar.setNombre(persona.getNombre());
			 personaEditar.setGenero(persona.getGenero());
			 personaEditar.setNucleoFamiliar(persona.getNucleoFamiliar());
			 personaEditar.setRolesFamilia(persona.getRolesFamilia());
			 return iPersonaService.editarPersona(personaEditar,response);
		}else {
			return per;
		}
	}
	
	@ApiOperation(value = "Eliminar una persona")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al eliminar la persona con id")})
	@DeleteMapping("/eliminarPersona/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Integer id,HttpServletResponse response) throws IOException {
		return iPersonaService.eliminarPersona(id,response);
	}
	
	@ApiOperation(value = "Muestra todas las personas con cierto ID")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 404, message = "Búsqueda sin resultados"),
			@ApiResponse(code = 400, message = "Error al obtener personas con id") })
	@GetMapping("/obtenerPersona/{id}")
	public ResponseEntity<?> obtenerPersona(@PathVariable Integer id,HttpServletResponse response) throws IOException {
		return iPersonaService.obtenerPersonaPorId(id, response);
	}
	
	
	@ApiOperation(value = "Crear un rol familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al crear rol familiar") })
	@PostMapping("/crearRolFamiliar")
	public ResponseEntity<?> crearRolFamilia(@RequestBody RolFamilia rolFamilia,HttpServletResponse response) throws IOException {
		rolFamilia.setFechaCreacion(new Date());
		return iRolFamiliaService.crearRolFamilia(rolFamilia, response);
	}
	
	
	@ApiOperation(value = "Editar un rol familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al actualizar rol familiar"),
			@ApiResponse(code = 404, message = "No existe rol familiar con este id")})
	@PutMapping("/editarRolFamilia/{id}")
	public ResponseEntity<?> editar(@RequestBody RolFamilia rolFamilia, @PathVariable Integer id,HttpServletResponse response) throws IOException{
		ResponseEntity<?> rolFam = iRolFamiliaService.obtenerRolFamiliaPorId(id,response);
		RolFamilia rolFamiliaEditar = (RolFamilia) rolFam.getBody();
		if(rolFamiliaEditar != null) {
			rolFamiliaEditar.setRol(rolFamilia.getRol());
			 return iRolFamiliaService.editarRolFamilia(rolFamiliaEditar, response);
		}else {
			return rolFam;
		}
	}
	
	@ApiOperation(value = "Eliminar un rol familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al eliminar un rol familiar con id")})
	@DeleteMapping("/eliminarRolFamilia/{id}")
	public ResponseEntity<?>eliminarRolFamilia(@PathVariable Integer id,HttpServletResponse response) throws IOException {
		return iRolFamiliaService.eliminarFamilia(id,response);
	}
	
	
	@ApiOperation(value = "Crear un núcleo familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al crear un núcleo familiar") })
	@PostMapping("/crearNucleoFamiliar")
	public  ResponseEntity<?> crearNexoFamiliar(@RequestBody NucleoFamiliar nucleoFamiliar,HttpServletResponse response) throws IOException {
		nucleoFamiliar.setFechaCreacion(new Date());
		return iNucleoFamiliarService.crearNucleoFamiliar(nucleoFamiliar,response);
	}
	
	
	@ApiOperation(value = "Editar un núcleo familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al actualizar un núcleo familiar"),
			@ApiResponse(code = 404, message = "No existe núcleo familiar con este id")})
	@PutMapping("/editarNucleoFamilia/{id}")
	public  ResponseEntity<?> editarNuceloFamilia(@RequestBody NucleoFamiliar nucleoFamiliar, @PathVariable Integer id,HttpServletResponse response) throws IOException{
		ResponseEntity<?> nucFam = iNucleoFamiliarService.obtenerNucleoFamiliarPorId(id,response);
		NucleoFamiliar nucleoFamiliarEditar = (NucleoFamiliar) nucFam.getBody();
		if(nucleoFamiliarEditar != null) {
			nucleoFamiliarEditar.setApellido(nucleoFamiliar.getApellido());
			 return iNucleoFamiliarService.editarNucleoFamiliar(nucleoFamiliarEditar, response);
		}else {
			return nucFam;
		}
		
	}
	
	@ApiOperation(value = "Eliminar un núcleo familiar")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transacción Exitosa"),
			@ApiResponse(code = 400, message = "Error al eliminar un núcleo familiar con id")})
	@DeleteMapping("/eliminarNucleoFamilia/{id}")
	public  ResponseEntity<?> eliminarNucleoFamilia(@PathVariable Integer id,HttpServletResponse response) throws IOException {
		return iNucleoFamiliarService.eliminarNucleoFamiliar(id,response);
	}
	

	
}
