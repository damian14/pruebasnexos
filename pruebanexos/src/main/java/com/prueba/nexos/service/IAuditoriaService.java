package com.prueba.nexos.service;

import com.prueba.nexos.model.Auditoria;

public interface IAuditoriaService {
	
	public Auditoria guardarAuditoria(Auditoria auditoria);

}
