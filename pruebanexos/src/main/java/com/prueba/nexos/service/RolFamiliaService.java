package com.prueba.nexos.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.prueba.nexos.model.Auditoria;
import com.prueba.nexos.model.Persona;
import com.prueba.nexos.model.RolFamilia;
import com.prueba.nexos.repository.RolFamiliaRepository;

@Service
public class RolFamiliaService implements IRolFamiliaService {
	
	@Autowired
	RolFamiliaRepository rolFamiliaRepository;
	
	@Autowired
	AuditoriaService auditoriaService;
	
	private final static Logger log = LoggerFactory.getLogger(RolFamiliaService.class);

	@Override
	public  ResponseEntity<?> crearRolFamilia(RolFamilia rolFamilia,HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		RolFamilia rolFamiliaDev;
		try {
			rolFamiliaDev=rolFamiliaRepository.save(rolFamilia);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			mensaje="Error al crear Rol Familiar";
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		} 
		return new ResponseEntity<RolFamilia>(rolFamiliaDev, HttpStatus.OK);
	}

	@Override
	public  ResponseEntity<?> editarRolFamilia(RolFamilia rolFamilia,HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		RolFamilia rolFamiliaDev;
		try {
			rolFamiliaDev=rolFamiliaRepository.save(rolFamilia);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			mensaje="Error al editar Rol Familiar";
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		} 
		return new ResponseEntity<RolFamilia>(rolFamiliaDev, HttpStatus.OK);
	}

	@Override
	public  ResponseEntity<?> eliminarFamilia(Integer idRolFamilia,HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		try {
			rolFamiliaRepository.deleteById(idRolFamilia);
			mensaje = "Se elmino correctamente el rol";
			JSONObject objetoMensaje = new JSONObject();
			objetoMensaje.put("mensaje", mensaje);
			return new ResponseEntity<>(objetoMensaje.toString(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al eliminar el rol con id :" +idRolFamilia);
			auditoriaService.guardarAuditoria(auditoria);
			mensaje = "Error al eliminar el rol con id :" +idRolFamilia;
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		
	}

	@Override
	public  ResponseEntity<?> obtenerRolFamiliaPorId(Integer idRolFamilia,HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		RolFamilia rolFamilia;
		String mensaje = "";
		try {
			rolFamilia=rolFamiliaRepository.findById(idRolFamilia).orElse(null);
			if(rolFamilia == null) {
				mensaje = "No existe rol con este id : "+idRolFamilia;
				Auditoria auditoria =  new Auditoria();
				auditoria.setError("No existe rol con este id :"+idRolFamilia);
				auditoria.setFechaCreacion(new Date());
				auditoria.setServicioError("No existe rol con este id : "+idRolFamilia);
				auditoriaService.guardarAuditoria(auditoria);
				response.sendError(HttpStatus.NOT_FOUND.value(), mensaje);
			}
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al obtener la persona con el id: "+idRolFamilia);
			auditoriaService.guardarAuditoria(auditoria);
			mensaje="Error al obtener la persona con el id: "+idRolFamilia;
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<RolFamilia>(rolFamilia, HttpStatus.OK);
	}
	
	
	@ExceptionHandler(IllegalArgumentException.class)
	void respuestaStatus(HttpServletResponse response, HttpStatus ok, String mensaje) throws IOException {
		response.sendError(ok.value(), mensaje);
	}

}
