package com.prueba.nexos.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.prueba.nexos.model.Auditoria;
import com.prueba.nexos.model.NucleoFamiliar;
import com.prueba.nexos.model.Persona;
import com.prueba.nexos.repository.NucleoFamiliaRepository;

@Service
public class NucleoFamiliarService implements INucleoFamiliarService{
	
	@Autowired
	NucleoFamiliaRepository nucleoFamiliaRepository;
	
	@Autowired
	AuditoriaService auditoriaService;
	
	
	private final static Logger log = LoggerFactory.getLogger(NucleoFamiliarService.class);

	@Override
	public ResponseEntity<?> crearNucleoFamiliar(NucleoFamiliar nucleoFamiliar,HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		String mensaje = "";
		NucleoFamiliar nucleoFamDev;
		try {
			nucleoFamDev=nucleoFamiliaRepository.save(nucleoFamiliar);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			mensaje="Error al crear núcleo familiar";
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<NucleoFamiliar>(nucleoFamDev, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> editarNucleoFamiliar(NucleoFamiliar nucleoFamiliar,HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		String mensaje = "";
		NucleoFamiliar nucleoFamDev;
		try {
			nucleoFamDev=nucleoFamiliaRepository.save(nucleoFamiliar);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			mensaje="Error al actualizar núcleo familiar";
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<NucleoFamiliar>(nucleoFamDev, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> eliminarNucleoFamiliar(Integer idNucleoFamiliar,HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		String mensaje = "";
		try {
			nucleoFamiliaRepository.deleteById(idNucleoFamiliar);
			mensaje = "Se elminó correctamente el núcleo familiar";
			JSONObject objetoMensaje = new JSONObject();
			objetoMensaje.put("mensaje", mensaje);
			return new ResponseEntity<>(objetoMensaje.toString(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al eliminar el núcleo familiar con id :" +idNucleoFamiliar);
			auditoriaService.guardarAuditoria(auditoria);
			mensaje = "Error al eliminar el núcleo familiar con id :" +idNucleoFamiliar;
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		
	}

	@Override
	public ResponseEntity<?> obtenerNucleoFamiliarPorId(Integer idRolFaidNucleoFamiliarmilia,HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		NucleoFamiliar nucleoFamDev;
		String mensaje = "";
		try {
			nucleoFamDev=nucleoFamiliaRepository.findById(idRolFaidNucleoFamiliarmilia).orElse(null);
			if(nucleoFamDev == null) {
				mensaje = "No existe núcleo familiar con  id : "+idRolFaidNucleoFamiliarmilia;
				Auditoria auditoria =  new Auditoria();
				auditoria.setError("No existe núcleo familiar con id :"+idRolFaidNucleoFamiliarmilia);
				auditoria.setFechaCreacion(new Date());
				auditoria.setServicioError("No existe núcleo familiar con  id : "+idRolFaidNucleoFamiliarmilia);
				auditoriaService.guardarAuditoria(auditoria);
				response.sendError(HttpStatus.NOT_FOUND.value(), mensaje);
			}
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al obtener en núcleo familiar  con el id: "+idRolFaidNucleoFamiliarmilia);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<NucleoFamiliar>(nucleoFamDev, HttpStatus.OK);
	}

	
	@ExceptionHandler(IllegalArgumentException.class)
	void respuestaStatus(HttpServletResponse response, HttpStatus ok, String mensaje) throws IOException {
		response.sendError(ok.value(), mensaje);
	}

}
