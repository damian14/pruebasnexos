package com.prueba.nexos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.nexos.model.Auditoria;
import com.prueba.nexos.repository.AuditoriaRepository;

@Service
public class AuditoriaService implements IAuditoriaService{
	
	@Autowired
	AuditoriaRepository auditoriaRepository;

	@Override
	public Auditoria guardarAuditoria(Auditoria auditoria) {
		// TODO Auto-generated method stub
		return auditoriaRepository.save(auditoria);
	}
	
	

}
