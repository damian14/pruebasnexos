package com.prueba.nexos.service;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.prueba.nexos.model.NucleoFamiliar;

public interface INucleoFamiliarService {
	
	
	public ResponseEntity<?> crearNucleoFamiliar(NucleoFamiliar nucleoFamiliar,HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> editarNucleoFamiliar(NucleoFamiliar nucleoFamiliar,HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> eliminarNucleoFamiliar(Integer idNucleoFamiliar,HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> obtenerNucleoFamiliarPorId(Integer idRolFaidNucleoFamiliarmilia,HttpServletResponse response) throws IOException;

}
