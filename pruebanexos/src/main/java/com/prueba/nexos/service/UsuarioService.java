package com.prueba.nexos.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.prueba.nexos.model.UsuarioApp;
import com.prueba.nexos.repository.UsuarioRepository;

@Service
public class UsuarioService implements UserDetailsService, IService {

	// Inyectar el repositorio usuario
	@Autowired
	UsuarioRepository UsuarioRepository;
	
	private final static Logger log = LoggerFactory.getLogger(UsuarioService.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		UsuarioApp usuario = null;
		List grantList = new ArrayList();
		
		try {
			// Se implementa
			usuario = obtenerUsuario(username);
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
			grantList.add(grantedAuthority);
			return new User(usuario.getNombre(), usuario.getContrasena(), true, true, true, true, grantList);

		} catch (Exception e) {
			// TODO: handle exception
			String error = "Error en el login , no existe el usuario '" + username + "' en el sistema";
			log.error(e.getMessage()+e.getCause()+e.getLocalizedMessage());
			throw new UsernameNotFoundException(error);
		}

	}

	@Override
	public UsuarioApp obtenerUsuario(String nombre) {
		// TODO Auto-generated method stub
		return UsuarioRepository.findByNombre(nombre);
	}

}
