package com.prueba.nexos.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.prueba.nexos.model.Auditoria;
import com.prueba.nexos.model.Persona;
import com.prueba.nexos.repository.PersonaRepository;

@Service
public class PersonService implements IPersonaService{
	
	@Autowired
	PersonaRepository personaRepository;
	
	@Autowired
	AuditoriaService auditoriaService;
	
	
	private final static Logger log = LoggerFactory.getLogger(PersonService.class);

	
	@Override
	public ResponseEntity<?> obtenerPersonas(HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		List<Persona> listaPersona;
		String mensaje = "";
		try {
			listaPersona=(List<Persona>) personaRepository.findAll();
			if(listaPersona.isEmpty()) {
				mensaje = "Busqueda sin resultados";
				Auditoria auditoria =  new Auditoria();
				auditoria.setError(mensaje);
				auditoria.setFechaCreacion(new Date());
				auditoria.setServicioError(mensaje);
				auditoriaService.guardarAuditoria(auditoria);
				response.sendError(HttpStatus.NOT_FOUND.value(), mensaje);
			}
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al obtener personas");
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		} 
		return new ResponseEntity<List<Persona>>(listaPersona, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?>  crearPersona(Persona persona, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		Persona personaDev;
		try {
			personaDev=personaRepository.save(persona);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			mensaje="Error al crear Persona";
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<Persona>(personaDev, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<?> editarPersona(Persona persona, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		Persona personaDev;
		try {
			personaDev=personaRepository.save(persona);
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			mensaje = "Error al actualizar persona";
			auditoria.setServicioError(mensaje);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<Persona>(personaDev, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> obtenerPersonaPorId(Integer idPersona, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		Persona personaDev;
		String mensaje = "";
		try {
			personaDev=personaRepository.findById(idPersona).orElse(null);
			if(personaDev == null) {
				mensaje = "No existe personas con este id : "+idPersona;
				Auditoria auditoria =  new Auditoria();
				auditoria.setError("No existe personas con este id :"+idPersona);
				auditoria.setFechaCreacion(new Date());
				auditoria.setServicioError("No existe personas con este id : "+idPersona);
				auditoriaService.guardarAuditoria(auditoria);
				response.sendError(HttpStatus.NOT_FOUND.value(), mensaje);
			}
		} catch (DataAccessException e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al obtener la persona con el id: "+idPersona);
			auditoriaService.guardarAuditoria(auditoria);
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
		return new ResponseEntity<Persona>(personaDev, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<?> eliminarPersona(Integer idPersona, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String mensaje = "";
		try {
			personaRepository.deleteById(idPersona);
			mensaje = "Se elmino correctamente la persona";
			JSONObject objetoMensaje = new JSONObject();
			objetoMensaje.put("mensaje", mensaje);
			return new ResponseEntity<>(objetoMensaje.toString(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			Auditoria auditoria =  new Auditoria();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			auditoria.setError(errors.toString());
			auditoria.setFechaCreacion(new Date());
			auditoria.setServicioError("Error al eliminar la persona con id :" +idPersona);
			auditoriaService.guardarAuditoria(auditoria);
			mensaje="Error al eliminar la persona con id :" +idPersona;
			response.sendError(HttpStatus.BAD_REQUEST.value(), mensaje);
			log.error(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PostMapping("/crear")
	@ResponseStatus(HttpStatus.CREATED)
	public Persona crear(@RequestBody Persona persona) {
		return personaRepository.save(persona);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	void respuestaStatus(HttpServletResponse response, HttpStatus ok, String mensaje) throws IOException {
		response.sendError(ok.value(), mensaje);
	}



	
}
