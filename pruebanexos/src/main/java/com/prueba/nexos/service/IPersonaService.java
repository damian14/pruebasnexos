package com.prueba.nexos.service;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.prueba.nexos.model.Persona;

public interface IPersonaService {
	
	public ResponseEntity<?> obtenerPersonas(HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> crearPersona(Persona persona, HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> editarPersona(Persona persona , HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> obtenerPersonaPorId(Integer idPersona , HttpServletResponse response) throws IOException;
	
	public ResponseEntity<?> eliminarPersona(Integer idPersona , HttpServletResponse response) throws IOException;
	
}
