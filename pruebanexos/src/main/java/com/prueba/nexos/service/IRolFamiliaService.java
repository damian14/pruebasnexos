package com.prueba.nexos.service;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.prueba.nexos.model.RolFamilia;

public interface IRolFamiliaService {
	
	public  ResponseEntity<?> crearRolFamilia(RolFamilia rolFamilia,HttpServletResponse response) throws IOException;
	
	public  ResponseEntity<?> editarRolFamilia(RolFamilia rolFamilia,HttpServletResponse response) throws IOException;
	
	public  ResponseEntity<?> eliminarFamilia(Integer idRolFamilia,HttpServletResponse response) throws IOException;
	
	public  ResponseEntity<?> obtenerRolFamiliaPorId(Integer idRolFamilia,HttpServletResponse response) throws IOException;

}
